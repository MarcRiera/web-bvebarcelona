---
title: "Enllaços"
menu:
  main:
    name: "Enllaços"
    weight: 5
    url: "/ca/enllacos/"
---
Altres pàgines relacionades amb el Metro de Barcelona i l'OpenBVE:

&nbsp;

[OpenBVE Project](https://openbve-project.net) - Lloc web oficial de l'OpenBVE. ***En anglès.***

[BVE Worldwide](https://bveworldwide.forumotion.com/) - Fòrum sobre els simuladors BVE i OpenBVE on es poden trobar molts projectes en construcció. ***En anglès.***

[RailServe](https://www.railserve.com/) - Web amb infinitat d'enllaços a pàgines web sobre viatges en ferrocarril, modelisme, simuladors i recursos diversos. Hi ha moltes rutes per a BVE i OpenBVE. ***En anglès.***

&nbsp;

[TMB](https://www.tmb.cat) - Web de Transports Metropolitans de Barcelona.

[FGC](https://www.fgc.cat) - Web de Ferrocarrils de la Generalitat de Catalunya (opera les línies L6, L7 i L8 del metro).

[Fòrum del Transport Català](https://www.transport.cat) - Fòrum sobre la mobilitat a Catalunya (transport públic i privat).

