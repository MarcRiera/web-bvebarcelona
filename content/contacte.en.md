---
url: "contact"
title: "Contact"
menu:
  main:
    name: "Contact"
    weight: 6
    url: "/en/contact"
---
If you need help, have a suggestion or have found errors while using our OpenBVE contents, leave a message here and we will try to find a solution as soon as possible.

<form name="contact" netlify-honeypot="bot-field" netlify>
  <input type="text" name="bot-field" style="display:none; positon:absolute; left:-50000px" tabindex="-1" autocomplete="off"/>
  <label>Name</label>
  <input type="text" name="name" required>
  <label>Email</label>
  <input type="email" name="email" required>
  <label>Message</label>
  <textarea name="message" required></textarea>
  <button type="submit">Send</button>
</form>
