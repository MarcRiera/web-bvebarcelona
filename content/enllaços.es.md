---
url: "enlaces"
title: "Enlaces"
menu:
  main:
    name: "Enlaces"
    weight: 5
    url: "/es/enlaces/"
---
Otras páginas relacionadas con el Metro de Barcelona y OpenBVE:

&nbsp;

[OpenBVE Project](https://openbve-project.net) - Sitio web oficial de OpenBVE. ***En inglés.***

[BVE Worldwide](https://bveworldwide.forumotion.com/) - Foro sobre los simuladores BVE y OpenBVE donde se pueden encontrar muchos proyectos en construcción. ***En inglés.***

[RailServe](https://www.railserve.com/) - Web con infinidad de enlaces a páginas web sobre viajes en ferrocarril, modelismo, simuladores y recursos varios. Se pueden encontrar muchas rutas para BVE y OpenBVE. ***En inglés.***

&nbsp;

[TMB](https://www.tmb.cat) - Web de Transports Metropolitans de Barcelona.

[FGC](https://www.fgc.cat) - Web de Ferrocarrils de la Generalitat de Catalunya (opera las líneas L6, L7 y L8 del metro).

[Fòrum del Transport Català](https://www.transport.cat) - Foro sobre la mobilidad en Cataluña (transporte público y privado).
