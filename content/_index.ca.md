---
title: "Benvinguts!"
image: '/images/main/portada.png'
menu:
  main:
    name: "Inici"
    weight: 1
    url: "/ca"
---
Aquí trobareu tota la informació sobre les rutes i els trens del Metro de Barcelona per al simulador ferroviari OpenBVE.
