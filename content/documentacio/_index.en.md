---
type: "single"
url: "documentation"
title: "Documentation"
menu:
  main:
    name: "Documentation"
    weight: 3
    url: "/en/documentation/"
---
In this section you will find detailed information about many topics:

## OpenBVE

### Trains

[ Driving manual – Barcelona Metro 2000/3000/4000 Series ]( {{< relref "openbve-conduccio-serie-2000-3000-4000" >}} )
