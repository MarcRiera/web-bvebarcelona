---
url: "contacto"
title: "Contacto"
menu:
  main:
    name: "Contacto"
    weight: 6
    url: "/es/contacto"
---
Si necesitas ayuda, tienes sugerencias o has encontrado algún error al usar nuestros contenidos para OpenBVE, puedes escribir un mensaje aquí e intentaremos resolverlo lo antes posible.

<form name="contact" netlify-honeypot="bot-field" netlify>
  <input type="text" name="bot-field" style="display:none; positon:absolute; left:-50000px" tabindex="-1" autocomplete="off"/>
  <label>Nombre</label>
  <input type="text" name="name" required>
  <label>Correo electrónico</label>
  <input type="email" name="email" required>
  <label>Mensaje</label>
  <textarea name="message" required></textarea>
  <button type="submit">Enviar</button>
</form>
