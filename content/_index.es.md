---
title: "¡Bienvenido!"
image: '/images/main/portada.png'
menu:
  main:
    name: "Inicio"
    weight: 1
    url: "/es"
---
Aquí encontrarás toda la información sobre las rutas y los trenes del Metro de Barcelona para el simulador ferroviario OpenBVE.
