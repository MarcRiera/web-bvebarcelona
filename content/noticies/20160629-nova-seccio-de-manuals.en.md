---
url: "news/2016/06/29/new-manuals-section"
title: "New manuals section"
date: 2016-06-29T12:00:00+01:00
---
The "Manuals" section is now open, and you will find useful OpenBVE documentation in it. The first manual is on installing packages in the new OpenBVE version, which will be required in order to play the new L3 version available this week.

See you!
