---
url: "links"
title: "Links"
menu:
  main:
    name: "Links"
    weight: 5
    url: "/en/links/"
---
Other websites related to the Barcelona Metro and OpenBVE:

&nbsp;

[OpenBVE Project](https://openbve-project.net) - Official OpenBVE website.

[BVE Worldwide](https://bveworldwide.forumotion.com/) - Forum about BVE and OpenBVE where a lot of projects can be found.

[RailServe](https://www.railserve.com/) - Website full of links to pages about railway trips, model railways, train simulators and more. There are a lot of BVE and OpenBVE routes available in it.

&nbsp;

[TMB](https://www.tmb.cat) - Official website for Transports Metropolitans de Barcelona.

[FGC](https://www.fgc.cat) - Official website for Ferrocarrils de la Generalitat de Catalunya (operator of metro lines L6, L7 and L8).

[Fòrum del Transport Català](https://www.transport.cat) - Forum about transports in Catalonia (both public and private). <em><strong>In Catalan and Spanish.</strong></em>
