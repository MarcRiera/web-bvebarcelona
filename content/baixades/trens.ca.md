---
title: "Trens"
menu:
  main:
    name: "Trens"
    weight: 1
    url: "/ca/baixades/trens/"
    parent: "Baixades"
---
|||
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------- |
| ![2000](/images/trens/menu/2000.png) | {{% tabletextpadding %}}[Sèrie 2000 del Ferrocarril Metropolità de Barcelona](/ca/baixades/trens/serie-2000){{% /tabletextpadding %}} |
| ![3000](/images/trens/menu/3000.png) | {{% tabletextpadding %}}[Sèrie 3000 del Ferrocarril Metropolità de Barcelona](/ca/baixades/trens/serie-3000){{% /tabletextpadding %}} |
| ![4000](/images/trens/menu/4000.png) | {{% tabletextpadding %}}Sèrie 4000 del Ferrocarril Metropolità de Barcelona<br/><br/>*En preparació*{{% /tabletextpadding %}}         |
| ![9000](/images/trens/menu/9000.png) | {{% tabletextpadding %}}[Sèrie 9000 del Ferrocarril Metropolità de Barcelona](/ca/baixades/trens/serie-9000){{% /tabletextpadding %}} |
