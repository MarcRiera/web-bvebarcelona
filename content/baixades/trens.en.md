---
url: "downloads/trains"
title: "Trains"
menu:
  main:
    name: "Trains"
    weight: 1
    url: "/en/downloads/trains/"
    parent: "Downloads"
---

|||
| ------------------------------------ | ---------------------------------------------------------------------------------------------------------------- |
| ![2000](/images/trens/menu/2000.png) | {{% tabletextpadding %}}[Barcelona Metro 2000 Series](/en/downloads/trains/2000-series){{% /tabletextpadding %}} |
| ![3000](/images/trens/menu/3000.png) | {{% tabletextpadding %}}[Barcelona Metro 3000 Series](/en/downloads/trains/3000-series){{% /tabletextpadding %}} |
| ![4000](/images/trens/menu/4000.png) | {{% tabletextpadding %}}Barcelona Metro 4000 Series<br/><br/>*Coming soon*{{% /tabletextpadding %}}              |
| ![9000](/images/trens/menu/9000.png) | {{% tabletextpadding %}}[Barcelona Metro 9000 Series](/en/downloads/trains/9000-series){{% /tabletextpadding %}} |
