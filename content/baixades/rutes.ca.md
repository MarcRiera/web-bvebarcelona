---
title: "Rutes"
menu:
  main:
    name: "Rutes"
    weight: 1
    url: "/ca/baixades/rutes/"
    parent: "Baixades"
---

|||
| -------------------------------- | -------------------------------------------------------------------------- |
| ![L2](/images/rutes/menu/L2.png) | [![L2_termo](/images/rutes/menu/L2_termo.png)](/ca/baixades/rutes/linia-2) |
| ![L3](/images/rutes/menu/L3.png) | [![L3_termo](/images/rutes/menu/L3_termo.png)](/ca/baixades/rutes/linia-3) |
