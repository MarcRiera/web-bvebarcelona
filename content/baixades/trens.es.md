---
url: "descargas/trenes"
title: "Trenes"
menu:
  main:
    name: "Trenes"
    weight: 1
    url: "/es/descargas/trenes/"
    parent: "Descargas"
---

|||
| ------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------- |
| ![2000](/images/trens/menu/2000.png) | {{% tabletextpadding %}}[Serie 2000 del Ferrocarril Metropolitano de Barcelona](/es/descargas/trenes/serie-2000){{% /tabletextpadding %}} |
| ![3000](/images/trens/menu/3000.png) | {{% tabletextpadding %}}[Serie 3000 del Ferrocarril Metropolitano de Barcelona](/es/descargas/trenes/serie-3000){{% /tabletextpadding %}} |
| ![4000](/images/trens/menu/4000.png) | {{% tabletextpadding %}}Serie 4000 del Ferrocarril Metropolitano de Barcelona<br/><br/>*En preparación*{{% /tabletextpadding %}}          |
| ![9000](/images/trens/menu/9000.png) | {{% tabletextpadding %}}[Serie 9000 del Ferrocarril Metropolitano de Barcelona](/es/descargas/trenes/serie-9000){{% /tabletextpadding %}} |
