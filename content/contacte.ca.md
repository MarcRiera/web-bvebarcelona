---
title: "Contacte"
menu:
  main:
    name: "Contacte"
    weight: 6
    url: "/ca/contacte"
---
Si necessiteu ajuda, teniu suggeriments o trobeu errors mentre feu servir amb els nostres continguts a l’OpenBVE, podeu escriure un missatge aquí i ho intentarem resoldre el més aviat possible.

<form name="contact" netlify-honeypot="bot-field" netlify>
  <input type="text" name="bot-field" style="display:none; positon:absolute; left:-50000px" tabindex="-1" autocomplete="off"/>
  <label>Nom</label>
  <input type="text" name="name" required>
  <label>Correu electrònic</label>
  <input type="email" name="email" required>
  <label>Missatge</label>
  <textarea name="message" required></textarea>
  <button type="submit">Envia</button>
</form>
