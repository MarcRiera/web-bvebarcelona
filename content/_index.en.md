---
title: "Welcome!"
image: '/images/main/portada.png'
menu:
  main:
    name: "Home"
    weight: 1
    url: "/en"
---
Here you will find all the information about the Barcelona Metro routes and trains available for the OpenBVE train simulator.
